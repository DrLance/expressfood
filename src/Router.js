import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import Home from './components/Home';
import MainMenu from './components/MainMenu';
import FoodList from './components/FoodList';
import FoodInfo from './components/FoodInfo';
import CartList from './components/CartList';

class RouterComponent extends Component {
  render() {
    const MainNav = StackNavigator(
      {
        home: { screen: Home },
        mainmenu: { screen: MainMenu }
      },
      {
        headerMode: 'none'
      }
    );
    return <MainNav />;
  }
}

export default RouterComponent;
