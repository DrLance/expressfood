import { ADD_TO_CART, CLEAR_CART, REMOVE_ITEM_CART } from './types';

export const addToCart = value => {
  return dispatch => {
    dispatch({
      type: ADD_TO_CART,
      payload: value
    });
  };
};

export const clearCart = value => {
  return dispatch => {
    dispatch({
      type: CLEAR_CART,
      payload: value
    });
  };
};

export const removeItemCart = value => {
  return dispatch => {
    dispatch({
      type: REMOVE_ITEM_CART,
      payload: value
    });
  };
};
