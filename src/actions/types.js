export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'loging_user';
export const ADD_TO_CART = 'add_to_cart';
export const CLEAR_CART = 'clear_cart';
export const REMOVE_ITEM_CART = 'remove_item_cart';
