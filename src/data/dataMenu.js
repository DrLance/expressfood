export default [
  {
    id: '0',
    category: 'Category 00',
    name: 'Пицца',
    picture: 'pizza'
  },
  {
    id: '1',
    category: 'Category 10',
    name: 'Суши',
    picture: 'sushi'
  },
  {
    id: '2',
    category: 'Category 20',
    name: 'Бургеры',
    picture: 'burgers'
  },
  {
    id: '3',
    category: 'Category 30',
    name: 'Сендвичи',
    picture: 'sandwitch'
  },
  {
    id: '4',
    category: 'Category 40',
    name: 'Французкие ходдоги',
    picture: 'hotdog'
  },
  {
    id: '5',
    category: 'Category 50',
    name: 'Наборы/Cеты',
    picture: 'sets'
  },
  {
    id: '6',
    category: 'Category 50',
    name: 'Напидки',
    picture: 'drinks'
  }
];
