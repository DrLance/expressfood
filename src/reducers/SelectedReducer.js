import { ADD_TO_CART, CLEAR_CART, REMOVE_ITEM_CART } from '../actions/types';
import { _ } from 'lodash';

const INITIAL_STATE = {
  currentFood: []
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
        currentFood: [...state.currentFood, action.payload]
      };
    case CLEAR_CART:
      return {
        ...state,
        currentFood: []
      };
    case REMOVE_ITEM_CART:
      return {
        ...state,
        currentFood: _.filter(state.currentFood, v => {
          if (v.id_rem === action.payload) {
            console.log('====================================');
            console.log('====================================');
            console.log(
              _.filter(state.currentFood, v => {
                if (v.id_rem === action.payload) {
                  return false;
                }
                return true;
              })
            );
            return false;
          }
          return true;
        })
      };
    default:
      return state;
  }
};

export default reducer;
