import { combineReducers } from 'redux';
import MenuReducer from './MenuReducer';
import SelectedReducer from './SelectedReducer';
import FoodReducer from './FoodReducer';

export default combineReducers({
  menuReducer: MenuReducer,
  selectedReducer: SelectedReducer,
  foodReducer: FoodReducer
});
