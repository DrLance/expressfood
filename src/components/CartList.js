import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { _ } from 'lodash';
import { Container, Content, Text, Button, Left, Right, Body, Footer, Grid, Col } from 'native-base';
import CartItemList from './commons/CartItemList';
import { BackHeader } from './navbar/';
import { addToCart, removeItemCart } from '../actions/SelectedActions';

let newChoice = [];
let totalPrice = 0;

class CartList extends Component {
  componentWillMount() {
    this.groupByChoice(this.props.currentFood);
  }

  componentWillReceiveProps(nextProps) {
    this.groupByChoice(nextProps.currentFood);
  }

  componentWillUnmount() {
    for (let i = 0; i <= newChoice.length; i++) {
      newChoice.pop();
    }
    totalPrice = 0;
  }

  groupByChoice(value) {
    totalPrice = 0;
    newChoice = _.chain(value)
      .groupBy('id')
      .map((v, i) => {
        return {
          id: i,
          id_rem: _.get(_.find(v, 'id_rem'), 'id_rem'),
          name: _.get(_.find(v, 'name'), 'name'),
          price: _.get(_.find(v, 'price'), 'price'),
          picture: _.get(_.find(v, 'picture'), 'picture'),
          category_id: _.get(_.find(v, 'category_id'), 'category_id', 0),
          description: _.get(_.find(v, 'description'), 'description'),
          cnt: parseInt(v.length, 10),
          sum_price: parseFloat(_.get(_.find(v, 'price'), 'price')).toFixed(2) * v.length
        };
      })
      .orderBy('name')
      .value();

    _.forEach(newChoice, v => {
      totalPrice += parseFloat(v.sum_price);
    });

    console.log('====================================');
    console.log(newChoice);
    console.log('====================================');
  }

  renderContainer() {
    if (this.props.currentFood.length <= 0) {
      return (
        <Grid>
          <Col style={{ height: 500, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ alignSelf: 'center' }}>Ваш кошик порожній!</Text>
            <Text>Ви нічого не додали в кошик</Text>
            <Button
              full
              style={{ alignSelf: 'center', backgroundColor: '#FAC120' }}
              onPress={() => {
                Actions.mainMenu();
              }}>
              <Text>Повернутися в Меню</Text>
            </Button>
          </Col>
        </Grid>
      );
    }
    return (
      <Body>
        <FlatList
          contentContainerStyle={styles.contaier}
          data={newChoice}
          renderItem={({ item }) => (
            <CartItemList
              {...item}
              nameHeader={this.props.name}
              newChoice={newChoice}
              onPressMinus={() => this.onPressMinus(item)}
            />
          )}
          keyExtractor={item => item.id}
        />
      </Body>
    );
  }

  renderFooter() {
    if (this.props.currentFood.length > 0) {
      return (
        <Footer style={styles.containerFooter}>
          <Left />
          <Body>
            <Button transparent>
              <Text style={styles.txtStylePrice}>Оформить</Text>
            </Button>
          </Body>
          <Right style={{ right: 20 }}>
            <Text style={styles.txtStylePrice}>{totalPrice} ₴</Text>
          </Right>
        </Footer>
      );
    }
  }
  render() {
    return (
      <Container>
        <BackHeader name="Корзина" />
        <Content padder>{this.renderContainer()}</Content>
        {this.renderFooter()}
      </Container>
    );
  }
}

const mapStatToProps = state => {
  return {
    currentFood: state.selectedReducer.currentFood
  };
};

const styles = {
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  containerFooter: {
    backgroundColor: '#D5342C',
    alignItems: 'center'
  },
  imgStyle: {
    height: 120,
    width: 140
  },
  txtStyle: {
    fontFamily: 'Roboto',
    fontSize: 10
  },
  txtStylePrice: {
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#FAC120'
  }
};

export default connect(mapStatToProps, { addToCart, removeItemCart })(CartList);
