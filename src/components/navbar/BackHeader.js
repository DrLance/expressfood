import React from 'react';
import { Header, Button, Left, Icon, Text, Body, Right } from 'native-base';

const BackHeader = props => {
  return (
    <Header style={{ backgroundColor: '#D5342C' }}>
      <Left>
        <Button
          transparent
          onPress={() => {
            Actions.pop();
          }}>
          <Icon style={{ color: '#000' }} name="md-arrow-back" />
        </Button>
      </Left>
      <Body>
        <Text style={styles.textFontStyle}>{props.name}</Text>
      </Body>
    </Header>
  );
};

const styles = {
  textStyle: {
    position: 'absolute',
    left: 29,
    top: -5,
    height: '70%',
    width: '70%',
    fontSize: 13
  },
  textFontStyle: {
    fontFamily: 'Roboto'
  }
};

export { BackHeader };
