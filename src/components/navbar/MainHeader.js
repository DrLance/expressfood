import React, { PureComponent } from 'react';
import { withNavigation } from 'react-navigation';
import { Text, Header, Button, Left, Right, Body, Icon } from 'native-base';

class MainHeader extends PureComponent {
  render() {
    return (
      <Header style={{ backgroundColor: '#D5342C' }}>
        <Left>
          <Button
            transparent
            onPress={() => {
              this.props.navigation.navigate('mainmenu');
            }}>
            <Icon style={{ color: '#000', fontSize: 20 }} name="home" />
          </Button>
        </Left>
        <Body>
          <Text style={styles.textFontStyle}>{this.props.name}</Text>
        </Body>
        <Right>
          <Button
            transparent
            onPress={() => {
              Actions.cartList();
            }}>
            <Icon style={{ color: '#000', fontSize: 35 }} ios="ios-cart" android="md-cart" />
            <Text style={styles.textStyle}>{this.props.selectCount}</Text>
          </Button>
        </Right>
      </Header>
    );
  }
}

const styles = {
  textStyle: {
    position: 'absolute',
    left: 29,
    top: -5,
    height: '70%',
    width: '70%',
    fontSize: 13
  },
  textFontStyle: {
    fontFamily: 'Roboto'
  }
};

export default withNavigation(MainHeader);
