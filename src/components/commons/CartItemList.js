import React, { Component } from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux';
import { _ } from 'lodash';
import { View, Text, Button, Icon, Card, CardItem } from 'native-base';
import { addToCart, clearCart, removeItemCart } from '../../actions/SelectedActions';

class CartItemList extends Component {
  onPressPls() {
    this.props.addToCart({
      id: this.props.id,
      id_rem: Math.round(new Date() / 1000) + _.uniqueId('id_'),
      category_id: this.props.category_id,
      name: this.props.name,
      description: this.props.description,
      price: this.props.price,
      picture: this.props.picture
    });
  }

  onPressMinus() {
    /*let tmpCnt = parseInt(value.cnt, 10);
    tmpCnt--;
    if (tmpCnt > 0) {
      this.props.removeItemCart(value.id_rem);
    } else {
      console.log(tmpCnt);
    }*/

    this.props.removeItemCart(this.props.id_rem);
  }

  render() {
    return (
      <Card style={styles.containerStyle}>
        <CardItem>
          <Image source={{ uri: this.props.picture }} style={styles.imgStyle} />
          <Text style={styles.txtStyle}>{this.props.name}</Text>
          <View style={styles.containerBtnStyle}>
            <Button
              small
              transparent
              style={styles.btnStyle}
              onPress={this.onPressMinus.bind(this)}
            >
              <Icon name="md-remove" style={styles.iconSize} />
            </Button>
            <Text style={styles.txtStyle}>{this.props.cnt}</Text>
            <Button small transparent style={styles.btnStyle} onPress={this.onPressPls.bind(this)}>
              <Icon name="md-add" style={styles.iconSize} />
            </Button>
          </View>
        </CardItem>
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentFood: state.selectedReducer.currentFood
  };
};

const styles = {
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  containerBtnStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 10
  },
  imgStyle: {
    height: 65,
    width: 65
  },
  txtStyle: {
    fontFamily: 'Roboto',
    fontSize: 15,
    padding: 5
  },
  btnStyle: {
    backgroundColor: '#D5342C',
    paddingLeft: 5,
    paddingRight: 5
  },
  iconSize: {
    fontSize: 15,
    color: '#FAC120'
  }
};
export default connect(mapStateToProps, { addToCart, clearCart, removeItemCart })(CartItemList);
