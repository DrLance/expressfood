import React from 'react';
import { Image } from 'react-native';
import { Card, CardItem, Left, Body, Text, Right } from 'native-base';

const FoodCard = props => {
  return (
    <Card>
      <CardItem>
        <Left>
          <Body>
            <Text>{props.name}</Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem>
        <Image source={{ uri: props.picture }} style={styles.imgStyle} />
      </CardItem>
      <CardItem style={{ paddingTop: 0 }}>
        <Left>
          <Text style={styles.txtStyle}>Состав:</Text>
        </Left>
      </CardItem>
      <CardItem style={{ paddingTop: 0 }}>
        <Left>
          <Text style={styles.txtStyle}>{props.description}</Text>
        </Left>
      </CardItem>
      <CardItem>
        <Right>
          <Text style={styles.txtStylePrice}>{props.price}</Text>
        </Right>
      </CardItem>
    </Card>
  );
};

const styles = {
  imgStyle: {
    resizeMode: 'cover',
    width: null,
    height: 200,
    flex: 1
  },
  txtStyle: {
    fontFamily: 'Roboto',
    fontSize: 12
  },
  txtStylePrice: {
    fontFamily: 'Roboto',
    fontSize: 18,
    color: '#D5342C'
  }
};

export { FoodCard };
