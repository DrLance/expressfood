import React, { Component } from 'react';
import { Image } from 'react-native';
import { View, Text, Button } from 'native-base';

class MenuItem extends Component {
  render() {
    let img = '';
    switch (this.props.picture) {
      case 'pizza':
        img = require('../../img/pizza.jpg');
        break;
      case 'burgers':
        img = require('../../img/burgers.jpg');
        break;
      case 'sets':
        img = require('../../img/sets.jpg');
        break;
      case 'sandwitch':
        img = require('../../img/sandwitch.jpg');
        break;
      case 'sushi':
        img = require('../../img/sushi.jpg');
        break;
      case 'drinks':
        img = require('../../img/drinks.jpg');
        break;
      case 'hotdog':
        img = require('../../img/hotdog.jpg');
        break;

      default:
        break;
    }
    return (
      <View style={styles.containerStyle}>
        <Text style={styles.txtStyle}>{this.props.name}</Text>
        <Button
          transparent
          style={styles.imgStyle}
          onPress={() => {
            Actions.foodList({ name: this.props.name });
          }}>
          <Image style={styles.imgStyle} source={img} />
        </Button>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  imgStyle: {
    height: 150,
    width: '100%'
  },
  txtStyle: {
    color: '#FAC120',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 20
  }
};
export default MenuItem;
