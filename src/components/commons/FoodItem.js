import React, { Component } from 'react';
import { Image } from 'react-native';
import { View, Text, Button, Left } from 'native-base';

class FoodItem extends Component {
  render() {
    return (
      <View style={styles.containerStyle}>
        <Button
          transparent
          style={styles.imgStyle}
          onPress={() => {
            Actions.foodInfo(this.props);
          }}>
          <Image style={styles.imgStyle} source={{ uri: this.props.picture }} />
        </Button>
        <Left>
          <Text style={styles.txtStyle}>{this.props.name}</Text>
          <Text style={styles.txtStylePrice}>{this.props.price}</Text>
        </Left>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  imgStyle: {
    height: 120,
    width: 140
  },
  txtStyle: {
    fontFamily: 'Roboto',
    fontSize: 10
  },
  txtStylePrice: {
    fontFamily: 'Roboto',
    fontSize: 10,
    color: '#D5342C',
    marginBottom: 15
  }
};
export default FoodItem;
