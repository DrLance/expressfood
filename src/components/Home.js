import React, { Component } from 'react';
import { ImageBackground } from 'react-native';
import { Container, View, Text, Spinner } from 'native-base';

const splashscreen = require('../img/splash2.png');

class Home extends Component {
  componentWillMount() {
    setTimeout(() => {
      this.props.navigation.navigate('mainmenu');
    }, 1500);
  }

  render() {
    return (
      <Container>
        <ImageBackground source={splashscreen} style={styles.imgStyle}>
          <View style={styles.containerStyle}>
            <Text style={styles.txtPhoneStyle}>Швидка доставка їжі</Text>
            <Text style={styles.txtPhoneStyle}>888-888-888-88</Text>
            <View style={styles.containerFooterStyle}>
              <Text style={styles.txtStyle}>Ефективно використовувати час ..</Text>
              <Spinner color="#fff" />
            </View>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '130%'
  },
  containerFooterStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imgStyle: {
    flex: 1,
    height: null,
    width: null
  },
  txtPhoneStyle: {
    color: '#FAC120',
    fontSize: 25,
    backgroundColor: '#D5342C'
  },
  txtStyle: {
    color: '#FAC120',
    fontSize: 10,
    backgroundColor: '#D5342C'
  }
};

export default Home;
