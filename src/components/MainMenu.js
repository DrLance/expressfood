import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { Container, View, Content } from 'native-base';
import MainHeader from './navbar/MainHeader';
import MenuItem from './commons/MenuItem';

let selectCount = 0;

class MainMenu extends Component {
  componentWillMount() {
    if (this.props.selectedFood.currentFood !== 'undefined') {
      selectCount = this.props.selectedFood.currentFood.length;
    }
  }

  componentWillReceiveProps(nextProps) {
    selectCount = nextProps.selectedFood.currentFood.length;
  }

  render() {
    return (
      <Container>
        <MainHeader name="Меню" selectCount={selectCount} />
        <Content>
          <View style={styles.containerStyle}>
            <FlatList
              data={this.props.menuData}
              renderItem={({ item }) => <MenuItem {...item} />}
              keyExtractor={item => item.id}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    menuData: state.menuReducer,
    selectedFood: state.selectedReducer
  };
};

const styles = {
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#D5342C'
  }
};

export default connect(mapStateToProps)(MainMenu);
