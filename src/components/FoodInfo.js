import React, { Component } from 'react';
import { Container, Content, Right, Text, Button, Footer, View, Icon } from 'native-base';
import { connect } from 'react-redux';
import { _ } from 'lodash';
import { BackHeader } from './navbar';
import { FoodCard } from './commons';
import { addToCart } from '../actions/SelectedActions';

class FoodInfo extends Component {
  constructor() {
    super();
    this.state = {
      currentCount: 1
    };
  }
  onPressAdd() {
    for (let i = 0; i < this.state.currentCount; i++) {
      this.props.addToCart({
        id: this.props.id,
        id_rem: Math.round(new Date() / 1000) + _.uniqueId('id_'),
        category_id: this.props.category_id,
        name: this.props.name,
        description: this.props.description,
        price: this.props.price,
        picture: this.props.picture
      });
    }
    Actions.pop();
  }

  onPlusCount() {
    this.setState({ currentCount: this.state.currentCount + 1 });
  }

  onMinusCount() {
    if (this.state.currentCount <= 1) {
      this.setState({ currentCount: 1 });
    } else {
      this.setState({ currentCount: this.state.currentCount - 1 });
    }
  }

  render() {
    return (
      <Container>
        <BackHeader name={this.props.nameHeader} />
        <Content padder>
          <FoodCard {...this.props} />
        </Content>
        <Footer style={{ backgroundColor: '#D5342C' }}>
          <View style={styles.cardStyle}>
            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 5 }}>
              <Button style={styles.btnStyle} onPress={this.onMinusCount.bind(this)}>
                <Icon name="md-remove" style={styles.iconSize} />
              </Button>
              <Button transparent>
                <Text style={{ fontSize: 19, color: '#000', left: 10, paddingBottom: 5 }}>
                  {this.state.currentCount}
                </Text>
              </Button>
              <Button style={styles.btnStyle} onPress={this.onPlusCount.bind(this)}>
                <Icon name="md-add" style={styles.iconSize} />
              </Button>
            </View>
            <Right>
              <Button transparent onPress={this.onPressAdd.bind(this)}>
                <Text style={styles.txtFooterAddStyle}>В КОШИК</Text>
              </Button>
            </Right>
          </View>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedFood: state.selectedReducer
  };
};

const styles = {
  cardStyle: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  txtFooterStyle: {
    color: '#000',
    fontSize: 20,
    alignSelf: 'center',
    paddingBottom: 5,
    paddingLeft: 2
  },
  txtFooterAddStyle: {
    color: '#FAC120',
    fontSize: 18
  },
  btnStyle: {
    left: 10,
    backgroundColor: '#D5342C'
  },
  iconSize: {
    fontSize: 25,
    color: '#FAC120'
  }
};

export default connect(mapStateToProps, { addToCart })(FoodInfo);
