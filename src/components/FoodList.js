import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { Container, View } from 'native-base';
import { MainHeader } from './navbar';
import FoodItem from './commons/FoodItem';

let selectCount = 0;

class FoodList extends Component {
  componentWillMount() {
    if (this.props.selectedFood.currentFood !== 'undefined') {
      selectCount = this.props.selectedFood.currentFood.length;
    }
  }

  componentWillReceiveProps(nextProps) {
    selectCount = nextProps.selectedFood.currentFood.length;
  }

  render() {
    return (
      <Container>
        <MainHeader name={this.props.name} selectCount={selectCount} />
        <View style={styles.containerStyle}>
          <FlatList
            contentContainerStyle={styles.contaier}
            data={this.props.foodData}
            renderItem={({ item }) => <FoodItem {...item} nameHeader={this.props.name} />}
            keyExtractor={item => item.id}
          />
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    foodData: state.foodReducer,
    selectedFood: state.selectedReducer
  };
};

const styles = {
  containerStyle: {
    flex: 1,
    paddingTop: 10
  },
  contaier: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    left: -8
  }
};

export default connect(mapStateToProps)(FoodList);
